{{-- Lista filmów z filtracją --}}

@foreach ($movies as $key => $movie)
    <div>
        <p>
            <a href="{{route('movie', $movie)}}">{{$movie->title}}</a>
        </p>
        <img src="{{'/uploads/posters/'.$movie->poster}}" alt="Italian Trulli" style="max-width: 150px;">
    </div>
    <br /><br />
@endforeach
