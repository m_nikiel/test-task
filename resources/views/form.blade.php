{{-- Strona z formularzem dodawania filmu --}}

@if(session('success'))
        <p>{{ session('success') }}</p><br />
@endif

@if(isset($errors) && count($errors))
    @foreach($errors->all() as $error)
        <p class="my-0">{{ $error }}</p><br />
    @endforeach
@endif



<form method="POST" action="{{route('form.send')}}" enctype="multipart/form-data">
    @csrf

    <label for="title">Tytuł</label><br>
    <input type="text" name="title"><br />

    <label for="year">Rok produkcji</label><br>
    <input type="text" name="year"><br />

    <label for="type">Typ materiału</label><br>
    <select name="type">
        <option value="">-- Wybierz typ --</option>
        <option value="movie">Film</option>
        <option value="series">Serial</option>
    </select><br />

    <label for="seasons">Liczba sezonów</label><br>
    <input type="text" name="seasons"><br />

    <label for="episodes">Liczba odcinków</label><br>
    <input type="text" name="episodes"><br />

    <label for="duration">Czas trwania</label><br>
    <input type="text" name="duration"><br />

    <label for="description">Opis</label><br>
    <textarea name="description"></textarea><br />

    <input type="file" name="poster"><br />

     <input type="submit">
</form>
